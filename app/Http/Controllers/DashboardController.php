<?php

namespace App\Http\Controllers;

use App\Models\Broadcast;
use App\Models\Member;
use App\Models\PenerimaBroadcast;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Helpers\WhatsappHelpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Exception;
use Ramsey\Uuid\Uuid;

class DashboardController extends Controller
{
    public function login()
    {
        return view('admin/auth-login');
    }

    public function register()
    {
        return view('auth/registerakun');
    }

    public function simpanadmin(Request $request)
    {
        $request->validate([
            'namalengkap' => 'required',
            'email' => 'required|email:dns',
            'password' => 'required',
        ]);

        try {
           if ($request->password==$request->konfirmasipassword)
           {
            User::Create([
                'name'=>$request->namalengkap,
                'email'=>$request->email,
                'password'=>Hash::make($request->password)
            ]);
            return response()->json(['status'=>'200','success'=>'Pendaftaran Berhasil']);
           }else
           {
            return response()->json(['status'=>'201','success'=>'Password dan Konfirmasi Password Tidak Sama']);
           }

        } catch (Exception $e) {
            return response()->json(['status'=>'201','success'=>$e->getMessage()]);
        }
    }

    public function cekAdmin(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email:dns'],
            'password' => ['required']
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/dashboard')->with('success', 'Selamat Datang Admin ...');

        }

        return redirect('/akun')->with('error', 'Login Tidak Berhasil .. ');
    }

    public function index()
    {
        return view('admin/dashboard');
    }

    public function datamember()
    {
        return view('admin/datamember');
    }
    public function listmember(Request $request,$id)
    {

        return view('admin/listmember');
    }


    public function sebarpesan()
    {
        return view('admin/broadcast');
    }
    public function formbroadcast()
    {
        return view('admin/form/broadcast');
    }

    public function logpesan(Request $request)
    {
        if($request->ajax()) {
            Carbon::setLocale(app()->getLocale());
            $getData=PenerimaBroadcast::select(DB::raw('id,idmember,idbroadcast,status,responpengiriman,created_at'))
                ->get();
            $data=[];
            foreach ($getData as $item)
            {
                $data[]=[
                    'id'=>$item['id'],
                    'identitas'=>WhatsappHelpers::getIdentitas($item['idmember']),
                    'namabroadcast'=>WhatsappHelpers::getNamaBroadcast($item['idbroadcast']),
                    'status'=>$item['status'],
                    'tanggal'=>Carbon::parse($item['updated_at'])->format('H:i:s Y-m-d')
                ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);

        }
        return view('admin/monitoring');
    }

    public function getmember(Request $request)
    {
        if($request->ajax()) {
            $getData=Member::select(DB::raw('id,namalengkap,nomorwa,alamat'))
                ->get();
            $data=[];
            foreach ($getData as $item) {
                $data[] = [
                    'id' => $item['id'],
                    'namalengkap' => $item['namalengkap'],
                    'nomorwa' => $item['nomorwa'],
                    'alamat' => $item['alamat']
                ];
            }
            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);

        }


        //return view('admin/datamember');

    }

    public function getbroadcast(Request $request)
    {
        if($request->ajax()) {
            $getData=Broadcast::select(DB::raw('id,namabroadcast,pesanbroadcast,jumlahpenerima,is_start'))
                ->get();
            $data=[];
            foreach ($getData as $item)
            {
                $data[]=[
                    'id'=>$item['id'],
                    'namabroadcast'=>$item['namabroadcast'],
                    'pesanbroadcast'=>$item['pesanbroadcast'],
                    'jumlahpenerima'=>WhatsappHelpers::getJumlahPenerima($item['id']),
                    'is_start'=>$item['is_start']
                ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);

        }


        return view('admin/broadcast');

    }

    public function sendwa()
    {
        try {
            $message='Tes Dev';
            $cekno=WhatsappHelpers::checkNomor('6285220717928');
            $respon=json_decode($cekno,true);
            if ($respon['message']=='Valid WhatsApp Number')
            {
                $kirim=WhatsappHelpers::kirimWA('6285220717928',$message);
                return response()->json(['status'=>'200','success'=>'Whatsapp Sukses Dikirim']);
            }else
            {
                return response()->json(['status'=>'200','success'=>'Nomor Whatsapp Tidak Dikenali']);
            }


        } catch (Exception $e) {
            return response()->json(['status'=>'201','success'=>$e->getMessage()]);
        }
    }

    public function simpanbroadcast(Request $request)
    {
        $request->validate([
            'namabroadcast' => 'required',
            'pesanbroadcast' => 'required'
        ]);

        try {

            $id=Uuid::uuid4()->toString();
            Broadcast::Create([
                    'id' => $id,
                    'namabroadcast' => $request->namabroadcast,
                    'pesanbroadcast' => $request->pesanbroadcast,
                    'jumlahpenerima'=>'0',
                    'is_start' => '0'
                ]);

                return response()->json(['status'=>'200','success'=>'Data Broadcast Berhasil Dibuat']);


        } catch (Exception $e) {
            return response()->json(['status'=>'201','success'=>$e->getMessage()]);
        }
    }

    public function selectMember(Request $request)
    {
        try {
            $pilih=$request->input('id_member');
            if (is_null($pilih))
            {
                return response()->json(['status'=>'200','success'=>'Member Tidak Ada Yang Pilih']);
            }else
            {
                $tags = explode(',',$pilih);
                $id_broadcast=$request->input('id_broadcast');
                foreach($tags as $key) {
                    $id_str=Uuid::uuid4()->toString();
                    $recordInsert = [
                        'id'=>$id_str,
                        'idmember' => $key,
                        'idbroadcast' => $id_broadcast,
                        'status' => '0',
                        'responpengiriman' => '-',

                    ];
                    $post=PenerimaBroadcast::create($recordInsert);

                }
                return response()->json(['status'=>'200','success'=>'Member Berhasil Dipilih']);

            }

        } catch (Exception $e) {
            return response()->json(['status'=>'201','success'=>$e->getMessage()]);
        }


    }

}
