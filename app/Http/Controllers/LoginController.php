<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use Ramsey\Uuid\Uuid;
use App\Helpers\WhatsappHelpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }

    public function index()
    {
        return view('auth/login');
    }

    public function keluar(Request $request): \Illuminate\Routing\Redirector|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse
    {
        Session::flush();
        Auth::logout();
        return redirect('/')->with('success', 'Anda Berhasil Logout ..');
    }

    public function register(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        return view('auth/register');
    }

    public function generatePin($length = 4)
    {
    $random = "";
    srand((double) microtime() * 1000000);
    $data = "123456123456789071234567890890";
    // $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz"; // if you need alphabatic also
    for ($i = 0; $i < $length; $i++) {
          $random .= substr($data, (rand() % (strlen($data))), 1);
    }
    return $random;
    }

    public function simpanmember(Request $request)
    {
        $request->validate([
            'namalengkap' => 'required',
            'nomorwa' => 'required|unique:members|starts_with:0|min:10'
        ]);

        try {
            $gen_satu=$this->generatePin();
            $hp=$request->nomorwa;

            $kode=$gen_satu.''.substr($hp,-4);

            $id=Uuid::uuid4()->toString();

            $message='Selamat Anda berhasil Terdaftar dengan Kode Masuk :'.$kode.' gunakan di alamat '.url('/');
            $str_to_replace = '62';
            $nowa = $str_to_replace . substr($request->nomorwa, 1);

            $cekno=WhatsappHelpers::checkNomor($nowa);
            $respon=json_decode($cekno,true);
            if ($respon['message']=='Valid WhatsApp Number')
            {
                $kirim=WhatsappHelpers::kirimWA($nowa,$message);
                Member::Create([
                    'id' => $id,
                    'namalengkap' => $request->namalengkap,
                    'nomorwa' => $request->nomorwa,
                    'alamat' => '-',
                    'desa' => '-',
                    'prov' => '-',
                    'kab' => '-',
                    'kec' => '-',
                    'otp' => $kode,
                    'is_status' => '1',
                ]);
                $request->session()->put([
                    'id' => $id,
                    'kode' => $kode,
                    'namalengkap' => $request->namalengkap,
                    'nomorwa' => $request->nomorwa,
                    'is_status' => '1',
                ]);



                return response()->json(['status'=>'200','success'=>'Whatsapp Sukses Dikirim']);
            }else
            {
                return response()->json(['status'=>'201','success'=>'Nomor Whatsapp Tidak Dikenali']);
            }
            return response()->json(['status'=>'200','success'=>'Nomor Whatsapp Valid']);
        } catch (Exception $e) {
            return response()->json(['status'=>'201','success'=>$e->getMessage()]);
        }
    }


    public function cekPin(Request $request)
    {
        $request->validate([
            'nomorwa' => 'required',
            'kode' => 'required'
        ]);
        $cekPIN=Member::select(DB::raw('*'))
        ->where('nomorwa',$request->nomorwa)
        ->where('otp',$request->kode);

        try {
            if($cekPIN->count()=='1')
            {
                $getData=$cekPIN->first();
                $request->session()->put([
                    'id' => $getData->id,
                    'kode' => $getData->kode,
                    'namalengkap' => $getData->namalengkap,
                    'nomorwa' => $getData->nomorwa,
                    'is_status' => $getData->is_status,
                ]);

                return redirect('/member')->with('success', 'Login Berhasil Masuk ..');

            }else
            {
                return redirect('/')->with('error', 'Nomor Whatsapp/Kode Tidak Dikenali');
            }

        } catch (Exception $e) {
            return redirect('/')->with('error', 'Terdapat Kesalahan '.$e->getMessage());
        }



    }

    public function updatemember(Request $request)
    {
       /*  $request->validate([
            'namalengkap' => 'required',
            'nomorwa' => 'required|starts_with:0|min:10',
            'alamat' => 'required',
            'desa' => 'required',
            'prov' => 'required',
            'kab' => 'required',
            'kec' => 'required',
        ]);
         */
        try {
            Member::updateOrCreate([
                'id'=>$request->id,
            ],[
                'namalengkap' => $request->namalengkap,
                'nomorwa' => $request->nomorwa,
                'alamat' => $request->alamat,
                'desa'=>$request->desa,
                'prov'=>$request->prov,
                'kab'=>$request->kab,
                'kec'=>$request->kec,
                'updated_at'=>Carbon::now(),
            ]);




            return response()->json(['status'=>'200','success'=>'Update Berhasil Valid']);
        } catch (Exception $e) {
            return response()->json(['status'=>'201','success'=>$e->getMessage()]);
        }
    }

}
