<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class MemberController extends Controller
{
    public function index()
    {
        $cekPIN=Member::select(DB::raw('*'))
        ->where('id',session('id'))
        ->first();

        $provinsi=DB::table('wilayah')
            ->where('id_negara','=','ID')
            ->where('nama_wilayah','like','Prov.%')
            ->orderBy('nama_wilayah')
            ->get();
        $kabupaten=DB::table('wilayah')
        ->Where(function ($query) {
            $query->where('nama_wilayah', 'like', 'Kota%')
                  ->orwhere('nama_wilayah', 'like', 'Kab.%');
        })
        ->orderBy('nama_wilayah')
        ->get();

        $kecamatan=DB::table('wilayah')
        ->where('nama_wilayah', 'like', 'Kec.%')
        ->orderBy('nama_wilayah')
        ->get();

        return view('member/dashboard',[
            "data"=>$cekPIN,
            "provinsi"=>$provinsi,
            "kabupaten"=>$kabupaten,
            "kecamatan"=>$kecamatan
        ]);
    }

    public function wilayahProvinsi()
    {
        $provinsi=DB::table('wilayah')
        ->where('id_negara','=','ID')
        ->where('nama_wilayah','like','Prov.%')
        ->orderBy('nama_wilayah')
        ->get();

        /* $getWilayahProvinsi = new NeoFeeder([
            'act' => 'GetWilayah',
            'filter' => "id_negara = 'ID'",
            'filter' => "nama_wilayah like 'Prov.%'",
            'order' => "nama_wilayah"
        ]); */

        return Response()->json([
            'error_code'=>0,
            'error_desc'=>'',
            'data'=>$provinsi,
            'message'=>'fetch data berhasil'
        ], 200);

    }

    public function wilayahKota(Request $request)
    {
        $idProvinsi = Str::substr($request->provinsi, 0, 2);
        $provinsi=DB::table('wilayah')
        ->where('id_wilayah','like',$idProvinsi.'%')
        ->Where(function ($query) {
            $query->where('nama_wilayah', 'like', 'Kota%')
                  ->orwhere('nama_wilayah', 'like', 'Kab.%');
        })
        ->orderBy('nama_wilayah')
        ->get();


        return Response()->json([
            'error_code'=>0,
            'error_desc'=>'',
            'data'=>$provinsi,
            'message'=>'fetch data berhasil'
        ], 200);

       /*  $getWilayahKota = new NeoFeeder([
            'act' => 'GetWilayah',
            'filter' => "id_wilayah like '$idProvinsi%' and (nama_wilayah like 'Kota%' or nama_wilayah like 'Kab.%')",
            'order' => "nama_wilayah"
        ]); */

    }

    public function wilayahKecamatan(Request $request)
    {
        $idKota = Str::substr($request->kabupaten, 0, 4);

        $provinsi=DB::table('wilayah')
        ->where('id_wilayah','like',$idKota.'%')
        ->where('nama_wilayah', 'like', 'Kec.%')
        ->orderBy('nama_wilayah')
        ->get();


        return Response()->json([
            'error_code'=>0,
            'error_desc'=>'',
            'data'=>$provinsi,
            'message'=>'fetch data berhasil'
        ], 200);
        

        /* $getWilayahKota = new NeoFeeder([
            'act' => 'GetWilayah',
            'filter' => "id_wilayah like '$idKota%' and nama_wilayah like 'Kec.%'",
            'order' => "nama_wilayah"
        ]); */

    }
}
