<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WhatsappHelpers {
    public static function kirimWAS($phone,$message)
    {
        $token = "eHzE3NaxgfYTD3jgNFRzfSof9U2dsRWTLs6x4H2YMV1SRwHQkP";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://app.ruangwa.id/api/send_message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'token='.$token.'&number='.$phone.'&message='.$message,
            ));
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            $response = curl_exec($curl);
            curl_close($curl);
            return $response;

    }

    public static function kirimWA($phone,$message)
    {
        $datasend = [
            "api_key" => "OU6TII4TCY2IGPMD",
            "number_key" => "arctKUiIU5asA2Vf",
            "phone_no" => $phone,
            "message" => $message,
        ];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.watzap.id/v1/send_message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode($datasend),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Cookie: PHPSESSID=6b74sv2ov0iov2jhteeq28kdtd; X_URL_PATH=aHR0cHM6Ly9jb3JlLndhdHphcC5pZC98fHx8fHN1c3VrYWNhbmc%3D'
            ),
            ));
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($curl);
            curl_close($curl);
            return $response;

    }

    public static function checkNomor($phone)
    {

        $datasend = [
            "api_key" => "OU6TII4TCY2IGPMD",
            "number_key" => "FqIy84R4fjNI9Fq6",
            "phone_no" => $phone,
        ];


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.watzap.id/v1/validate_number',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($datasend),
            ));

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }


    public static function kirimWAImage($phone,$message)
    {
        $dataSending = Array();
        $dataSending["api_key"] = "OU6TII4TCY2IGPMD";
        $dataSending["number_key"] = "ickSzuUJuSwtfFxb";
        $dataSending["phone_no"] = $phone;
        $dataSending["message"] = $message;
        $dataSending["url"] = "https://yuyunhidayat.id/selamat_lulus.jpeg";
        $dataSending["separate_caption"]="0";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.watzap.id/v1/send_image_url',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode($dataSending),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Cookie: PHPSESSID=6b74sv2ov0iov2jhteeq28kdtd; X_URL_PATH=aHR0cHM6Ly9jb3JlLndhdHphcC5pZC98fHx8fHN1c3VrYWNhbmc%3D'
            ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            return $response;

    }

    public static function kirimWAButton($phone)
    {
        $token = "eHzE3NaxgfYTD3jgNFRzfSof9U2dsRWTLs6x4H2YMV1SRwHQkP";
        $text= "Testing kirim button";
        $buttonlabel= "Google,Facebook";
        $buttonurl= "https://www.google.com,https://www.facebook.com";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://app.ruangwa.id/api/send_buttonurl',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'token='.$token.'&number='.$phone.'&text='.$text.'&buttonlabel='.$buttonlabel.'&buttonurl='.$buttonurl,
            CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/x-www-form-urlencoded'
                ),
        ));
            $response = curl_exec($curl);
            curl_close($curl);


            return $response;

    }



    public static function getJumlahPenerima($id)
    {
        $fakultas=DB::table('penerima_broadcasts')
        ->where('idbroadcast','=',$id)
        ->count();
            return $fakultas;


    }

    public static function getIdentitas($id)
    {
        $data=DB::table('members')
        ->where('id','=',$id)
        ->first();
        if (is_null($data))
        {
            return 'Tidak Ditemukan';
        }else
        {
            return $data->namalengkap.' ('.$data->nomorwa.')';
        }

    }

    public static function getNamaBroadcast($id)
    {
        $data=DB::table('broadcasts')
            ->where('id','=',$id)
            ->first();
        if (is_null($data))
        {
            return 'Tidak Ditemukan';
        }else
        {
            return $data->namabroadcast;
        }

    }
    public static function getDaftar($kode)
    {
        $tanggal=Carbon::parse(Carbon::now())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2024-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->where('kodeprodi_satu','=',$kode)
        ->where('pin','<>','')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();
        return $pendaftar;
    }

    public static function getLulus($kode)
    {
        $tanggal=Carbon::parse(Carbon::now())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2024-03-01');;
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->join('quiz_murid', 'quiz_murid.murid_id', '=', 'neomahasiswas.id')
        ->where('kodeprodi_satu','=',$kode)
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->where('quiz_murid.status','=','1')
        ->count();

        return $pendaftar;
    }

    public static function getDaftar2($kode)
    {
        $tanggal=Carbon::parse(Carbon::now()->subyear())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2023-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->where('kodeprodi_satu','=',$kode)
        ->where('pin','<>','')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();
        return $pendaftar;
    }

    public static function getLulus2($kode)
    {
        $tanggal=Carbon::parse(Carbon::now()->subyear())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2023-03-01');;
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->join('quiz_murid', 'quiz_murid.murid_id', '=', 'neomahasiswas.id')
        ->where('kodeprodi_satu','=',$kode)
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->where('quiz_murid.status','=','1')
        ->count();

        return $pendaftar;
    }

    public static function getPastDaftar($kode)
    {
        $tanggal=Carbon::parse(Carbon::now()->subYear())->format('Y-m-d');
        $pendaftar=DB::connection('mysql2')->table('pe3_formulir_pendaftaran')->select(DB::raw('*'))
        ->join('pe3_prodi', 'pe3_prodi.id', '=', 'pe3_formulir_pendaftaran.prodi_id')
        ->where('pe3_formulir_pendaftaran.prodi_id','=',$kode)
        ->whereDate('pe3_formulir_pendaftaran.created_at','<=',$tanggal)
        ->count();
        return $pendaftar;
    }

    public static function getPastLulus($kode)
    {
        $tanggal=Carbon::parse(Carbon::now()->subYear())->format('Y-m-d');
        $pendaftar=DB::connection('mysql2')->table('pe3_formulir_pendaftaran')->select(DB::raw('*'))
        ->join('pe3_prodi', 'pe3_prodi.id', '=', 'pe3_formulir_pendaftaran.prodi_id')
        ->join('pe3_nilai_ujian_pmb', 'pe3_nilai_ujian_pmb.user_id', '=', 'pe3_formulir_pendaftaran.user_id')
        ->where('pe3_formulir_pendaftaran.prodi_id','=',$kode)
        ->where('pe3_nilai_ujian_pmb.ket_lulus','=','1')
        ->whereDate('pe3_nilai_ujian_pmb.created_at','<=',$tanggal)
        ->count();

        return $pendaftar;
    }

    public static function getLulusFakultas($kode)
    {
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->join('quiz_murid', 'quiz_murid.murid_id', '=', 'neomahasiswas.id')
        ->whereIn('kodeprodi_satu',$kode)
        ->where('quiz_murid.status','=','1')
        ->count();

        return $pendaftar;
    }

    public static function getLulusPersen($kode)
    {
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->join('quiz_murid', 'quiz_murid.murid_id', '=', 'neomahasiswas.id')
        ->whereIn('kodeprodi_satu',$kode)
        ->where('quiz_murid.status','=','1')
        ->count();
        $persen=($pendaftar/250)*100;

        return number_format($persen,0).'%';
    }

    public static function getTotalDaftar()
    {
        $tanggal=Carbon::parse(Carbon::now())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2024-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->where('pin','<>','')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();
        return $pendaftar;
    }

    public static function getTotalDaftar2()
    {
        $tanggal=Carbon::parse(Carbon::now()->subyear())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2023-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->where('pin','<>','')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();
        return $pendaftar;
    }

    public static function getTotalLulus()
    {
        $tanggal=Carbon::parse(Carbon::now())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2024-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('neomahasiswas.id,pin, nama_mahasiswa,is_aktif,handphone,nama_prodi,nama_jenjang,nomor_pendaftaran,nilai'))
        ->join('pe3_prodi','neomahasiswas.kodeprodi_satu','=','pe3_prodi.config')
        ->join('quiz_murid','neomahasiswas.id','=','quiz_murid.murid_id')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();

        return $pendaftar;
    }

    public static function getTotalLulus2()
    {
        $tanggal=Carbon::parse(Carbon::now()->subyear())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2023-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('neomahasiswas.id,pin, nama_mahasiswa,is_aktif,handphone,nama_prodi,nama_jenjang,nomor_pendaftaran,nilai'))
        ->join('pe3_prodi','neomahasiswas.kodeprodi_satu','=','pe3_prodi.config')
        ->join('quiz_murid','neomahasiswas.id','=','quiz_murid.murid_id')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();

        return $pendaftar;
    }

    public static function getPastTotalDaftar()
    {
        $tanggal=Carbon::parse(Carbon::now()->subYear())->format('Y-m-d');
        $pendaftar=DB::connection('mysql2')->table('pe3_formulir_pendaftaran')->select(DB::raw('*'))
        ->join('pe3_prodi', 'pe3_prodi.id', '=', 'pe3_formulir_pendaftaran.prodi_id')
        ->where('pe3_formulir_pendaftaran.prodi_id','<>','24')
        ->whereDate('pe3_formulir_pendaftaran.created_at','<=',$tanggal)
        ->count();
        return $pendaftar;
    }

    public static function getPastTotalLulus()
    {
        $tanggal=Carbon::parse(Carbon::now()->subYear())->format('Y-m-d');
        $pendaftar=DB::connection('mysql2')->table('pe3_formulir_pendaftaran')->select(DB::raw('b.nama_prodi,b.nama_jenjang, nama_mhs,ket_lulus'))
        ->join('pe3_prodi', 'pe3_prodi.id', '=', 'pe3_formulir_pendaftaran.prodi_id')
        ->join('pe3_nilai_ujian_pmb', 'pe3_nilai_ujian_pmb.user_id', '=', 'pe3_formulir_pendaftaran.user_id')
        ->where('pe3_formulir_pendaftaran.prodi_id','<>','24')
        ->whereDate('pe3_nilai_ujian_pmb.created_at','<=',$tanggal)
        ->count();

        return $pendaftar;
    }

     public static function getTotalPin($kode)
    {
        $tanggal=Carbon::parse(Carbon::now())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2024-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->where('kodeprodi_satu','=',$kode)
        ->where('is_aktif','=','1')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();
        return $pendaftar;
    }

    public static function getTotalPin2($kode)
    {
        $tanggal=Carbon::parse(Carbon::now()->subyear())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2023-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->where('kodeprodi_satu','=',$kode)
        ->where('is_aktif','=','1')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();
        return $pendaftar;
    }

    public static function getJumlahPin()
    {
        $tanggal=Carbon::parse(Carbon::now())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2024-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->where('is_aktif','=','1')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();
        return $pendaftar;
    }

    public static function getJumlahPin2()
    {
        $tanggal=Carbon::parse(Carbon::now()->subyear())->format('Y-m-d');
        $tanggal2=Carbon::createFromFormat('Y-m-d', '2023-03-01');
        $pendaftar=Neomahasiswa::select(DB::raw('*'))
        ->where('is_aktif','=','1')
        ->whereDate('created_at','<=',$tanggal)
        ->whereDate('created_at','>=',$tanggal2)
        ->count();
        return $pendaftar;
    }

    public static function getFakultasNama($kode)
    {
        $fakultas=DB::table('pe3_fakultas')
        ->where('kode_fakultas','=',$kode)
        ->first();
        if (is_null($fakultas))
        {
            return 'Tidak Ditemukan !';
        }else
        {
            return $fakultas->nama_fakultas;
        }
    }

    public static function getSekolahKab($kode)
    {
        $fakultas=DB::table('sekolah')
        ->where('sekolah','=',$kode)
        ->first();
        return $fakultas->kabupaten_kota;
    }

    public static function public_path($path = null)
    {
        return rtrim(app()->basePath('public/' . $path), '/');
    }
    public static function exported_path($folder='/')
    {
        return app()->basePath("public/exported$folder");
    }

    public static function tanggal($format, $date=null) {
        Carbon::setLocale(app()->getLocale());
        if ($date == null){
            $tanggal=Carbon::parse(Carbon::now())->format($format);
        }else{
            $tanggal = Carbon::parse($date)->format($format);
        }
        $result = str_replace([
                                'Sunday',
                                'Monday',
                                'Tuesday',
                                'Wednesday',
                                'Thursday',
                                'Friday',
                                'Saturday'
                            ],
                            [
                                'Minggu',
                                'Senin',
                                'Selasa',
                                'Rabu',
                                'Kamis',
                                'Jumat',
                                'Sabtu'
                            ],
                            $tanggal);

        return str_replace([
                            'January',
                            'February',
                            'March',
                            'April',
                            'May',
                            'June',
                            'July',
                            'August',
                            'September',
                            'October',
                            'November' ,
                            'December'
                        ],
                        [
                            'Januari',
                            'Februari',
                            'Maret',
                            'April',
                            'Mei',
                            'Juni',
                            'Juli',
                            'Agustus',
                            'September',
                            'Oktober',
                            'November',
                            'Desember'
                        ], $result);
    }

    public static function tanggal1($format, $date=null) {
        Carbon::setLocale(app()->getLocale());
        if ($date == null){
            $tanggal=Carbon::parse(Carbon::now()->subyear())->format($format);
        }else{
            $tanggal = Carbon::parse($date)->format($format);
        }
        $result = str_replace([
                                'Sunday',
                                'Monday',
                                'Tuesday',
                                'Wednesday',
                                'Thursday',
                                'Friday',
                                'Saturday'
                            ],
                            [
                                'Minggu',
                                'Senin',
                                'Selasa',
                                'Rabu',
                                'Kamis',
                                'Jumat',
                                'Sabtu'
                            ],
                            $tanggal);

        return str_replace([
                            'January',
                            'February',
                            'March',
                            'April',
                            'May',
                            'June',
                            'July',
                            'August',
                            'September',
                            'October',
                            'November' ,
                            'December'
                        ],
                        [
                            'Januari',
                            'Februari',
                            'Maret',
                            'April',
                            'Mei',
                            'Juni',
                            'Juli',
                            'Agustus',
                            'September',
                            'Oktober',
                            'November',
                            'Desember'
                        ], $result);
    }

    public static function tanggal2($format, $date=null) {
        Carbon::setLocale(app()->getLocale());
        if ($date == null){
            $tanggal=Carbon::parse(Carbon::now()->subyear(2))->format($format);
        }else{
            $tanggal = Carbon::parse($date)->format($format);
        }
        $result = str_replace([
                                'Sunday',
                                'Monday',
                                'Tuesday',
                                'Wednesday',
                                'Thursday',
                                'Friday',
                                'Saturday'
                            ],
                            [
                                'Minggu',
                                'Senin',
                                'Selasa',
                                'Rabu',
                                'Kamis',
                                'Jumat',
                                'Sabtu'
                            ],
                            $tanggal);

        return str_replace([
                            'January',
                            'February',
                            'March',
                            'April',
                            'May',
                            'June',
                            'July',
                            'August',
                            'September',
                            'October',
                            'November' ,
                            'December'
                        ],
                        [
                            'Januari',
                            'Februari',
                            'Maret',
                            'April',
                            'Mei',
                            'Juni',
                            'Juli',
                            'Agustus',
                            'September',
                            'Oktober',
                            'November',
                            'Desember'
                        ], $result);
    }


}
