<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;
    protected $table = 'members';
	/**
	 * primary key tabel ini.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'id',
        'namalengkap',
		'nomorwa',
        'alamat',
        'desa',
        'prov',
        'kab',
		'kec',
        'otp',
        'is_status',
        'created_at',
        'updated_at',
	];

    public $incrementing = false;

    protected $casts = [
        'id'=>'string'
    ];
}
