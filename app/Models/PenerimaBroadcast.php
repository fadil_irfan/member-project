<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenerimaBroadcast extends Model
{
    use HasFactory;
    protected $table = 'penerima_broadcasts';
    /**
     * primary key tabel ini.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'idmember',
        'idbroadcast',
        'status',
        'responpengiriman',
        'created_at',
        'updated_at',
    ];

    public $incrementing = false;

    protected $casts = [
        'id'=>'string'
    ];
}
