<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Broadcast extends Model
{
    use HasFactory;
    protected $table = 'broadcasts';
    /**
     * primary key tabel ini.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'namabroadcast',
        'pesanbroadcast',
        'jumlahpenerima',
        'is_start',
        'created_at',
        'updated_at',
    ];

    public $incrementing = false;

    protected $casts = [
        'id'=>'string'
    ];
}
