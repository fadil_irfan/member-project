<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogPesan extends Model
{
    use HasFactory;
    protected $table = 'log_pesans';
    /**
     * primary key tabel ini.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'jenispengiriman',
        'status',
        'responpengiriman',
        'created_at',
        'updated_at',
    ];

    public $incrementing = false;

    protected $casts = [
        'id'=>'string'
    ];
}
