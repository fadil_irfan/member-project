<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\MemberController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
}); */


Route::get('/', [LoginController::class, 'index'])->name('login.index');
Route::get('/install', [LoginController::class, 'welcome'])->name('login.welcome');
Route::get('/register', [LoginController::class, 'register'])->name('login.register');

Route::get('/akun', [DashboardController::class, 'login'])->name('login.akun');
Route::get('/registerakun', [DashboardController::class, 'register'])->name('admin.register');
Route::post('/simpan-admin', [DashboardController::class, 'simpanadmin'])->name('admin.simpanadmin');


Route::post('/cekPin', [LoginController::class, 'cekPin'])->name('cek.pin');
Route::post('/simpan-member', [LoginController::class, 'simpanmember'])->name('login.simpanmember');
Route::post('/update-member', [LoginController::class, 'updatemember'])->name('login.updatemember');


Route::get('/keluar', [LoginController::class, 'keluar'])->name('login.keluar');

Route::get('/admin', [DashboardController::class, 'login'])->name('admin.index');
Route::post('/cekadmin', [DashboardController::class, 'cekAdmin'])->name('cek.admin');


Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
Route::get('/datamember', [DashboardController::class, 'datamember'])->name('dashboard.datamember');
Route::get('/listmember/{id}', [DashboardController::class, 'listmember'])->name('dashboard.listmember');
Route::post('/selectmember', [DashboardController::class, 'selectMember'])->name('dashboard.selectmember');

Route::get('/broadcast', [DashboardController::class, 'getbroadcast'])->name('dashboard.broadcast');
Route::post('/simpan-broadcast', [DashboardController::class, 'simpanbroadcast'])->name('dashboard.simpanbroadcast');

Route::get('/formbroadcast', [DashboardController::class, 'formbroadcast'])->name('dashboard.formbroadcast');

Route::get('/monitoring', [DashboardController::class, 'logpesan'])->name('dashboard.monitoring');

Route::get('/getmember', [DashboardController::class, 'getmember'])->name('dashboard.getmember');


Route::get('/sendwa', [DashboardController::class, 'sendwa'])->name('dashboard.kirimwa');


Route::get('/member', [MemberController::class, 'index'])->name('member.index');


Route::get('ref-wilayah-provinsi', [MemberController::class, 'wilayahProvinsi']);
Route::get('ref-wilayah-kota', [MemberController::class, 'wilayahKota']);
Route::get('ref-wilayah-kecamatan', [MemberController::class, 'wilayahKecamatan']);
