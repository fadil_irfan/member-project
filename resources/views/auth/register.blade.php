<!DOCTYPE html>
<html lang="en">
<head>
    <title>Register Member</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="Aplikasi Member">
    <meta name="author" content="Register">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <!-- FontAwesome JS-->
    <script defer src="{{ asset('assets/plugins/fontawesome/js/all.min.js') }}"></script>

    <!-- App CSS -->
    <link id="theme-style" rel="stylesheet" href="{{ asset('assets/css/portal.css') }}">

</head>

<body class="app app-signup p-0">
    <div class="row g-0 app-auth-wrapper">
	    <div class="col-12 col-md-7 col-lg-6 auth-main-col text-center p-5">
		    <div class="d-flex flex-column align-content-end">
			    <div class="app-auth-body mx-auto">
				    <div class="app-auth-branding mb-4"><a class="app-logo" href="index.html"><img class="logo-icon me-2" src="assets/images/app-logo.svg" alt="logo"></a></div>
					<h2 class="auth-heading text-center mb-4">Pendaftaran Member Baru</h2>

					<div class="auth-form-container text-start mx-auto">
						<form class="auth-form auth-signup-form" id="MemberForm" name="MemberForm">

							<div class="email mb-3">
								<label class="sr-only" for="signup-email">Nomor Whatsapp</label>
								<input id="phone" name="nomorwa" type="tel" class="form-control" placeholder="(555) 555-5555" required="required">
							</div>
							<div class="email mb-3">
								<label class="sr-only" for="signup-email">Nama Lengkap</label>
								<input id="namalengkap" name="namalengkap" type="text" class="form-control signup-name" placeholder="Nama Lengkap" required="required">
							</div>



							<div class="text-center">
								<button type="submit" id="saveBtn" class="btn app-btn-primary w-100 theme-btn mx-auto">Daftar</button>
							</div>
						</form><!--//auth-form-->

						<div class="auth-option text-center pt-5">Sudah Ada Akun ? <a class="text-link" href="{{ url('/') }}" >Masuk</a></div>
                        <p id="result"></p>
					</div><!--//auth-form-container-->



			    </div><!--//auth-body-->

			    <footer class="app-auth-footer">
				    <div class="container text-center py-3">
				         <!--/* This template is free as long as you keep the footer attribution link. If you'd like to use the template without the attribution link, you can buy the commercial license via our website: themes.3rdwavemedia.com Thank you for your support. :) */-->
			        <small class="copyright">@2024 by Kaisa Dev</small>

				    </div>
			    </footer><!--//app-auth-footer-->
		    </div><!--//flex-column-->
	    </div><!--//auth-main-col-->
	    <div class="col-12 col-md-5 col-lg-6 h-100 auth-background-col">
		    <div class="auth-background-holder">
		    </div>
		    <div class="auth-background-mask"></div>
		    <div class="auth-background-overlay p-3 p-lg-5">
			    <div class="d-flex flex-column align-content-end h-100">
				    <div class="h-100"></div>
				    <div class="overlay-content p-3 p-lg-4 rounded">
					    <h5 class="mb-3 overlay-title">Pendaftaran Member Baru</h5>
					    <div></div>
				    </div>
				</div>
		    </div><!--//auth-background-overlay-->
	    </div><!--//auth-background-col-->

    </div><!--//row-->


</body>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js">
</script>
<script type="text/javascript">
    var phoneInput = document.getElementById('phone');
    var result = document.getElementById('result');  // only for debugging purposes

    phoneInput.addEventListener('input', function (e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,6})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });


</script>

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

      $('#saveBtn').click(function (e) {
          phoneInput.value = phoneInput.value.replace(/\D/g, '');
          e.preventDefault();
          $('#saveBtn').html('Mengirim Data ..');
		  $('#saveBtn').attr('disabled','disabled');

          $.ajax({
            data: $('#MemberForm').serialize(),
            url: "simpan-member",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#MemberForm').trigger("reset");
                if (data.status=='200')
                {
                    Swal.fire(
                    'Pendaftaran Berhasil',
                    data.success,
                    'success'
                ).then(function (result) {
                if (result.value) {
                    window.location = "member";
                }
            })
                }else
                {
                    Swal.fire(
                    'Pendaftaran Tidak Berhasil',
                    data.success,
                    'error'
                ).then(function (result) {
                if (result.value) {

                }
            })
                }


                $('#saveBtn').html('Daftar');
				$('#saveBtn').removeAttr('disabled');

            },
            error: function (data) {
                //console.log('Error:', data);
                Swal.fire(
                    'Terdapat Kesalahan',
                    data.responseJSON.message,
                    'error'
                )
                $('#saveBtn').html('Daftar');
				$('#saveBtn').removeAttr('disabled');
            }
        });
      });


    });
  </script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js"></script>
    @include('helpers.toast')
</html>

