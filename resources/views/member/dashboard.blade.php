@extends('member.layout')
@section('content')
<div class="app-content pt-3 p-md-3 p-lg-4">
		    <div class="container-xl">
			    
			    <h1 class="app-page-title">Form Data Member</h1>
			
	
			    <div class="row g-4 mb-4">
			        <div class="col-12 col-lg-12">
				        <div class="app-card app-card-chart h-100 shadow-sm">
					        <div class="app-card-header p-3">
						        <div class="row justify-content-between align-items-center">
							        <div class="col-auto">
						                <h4 class="app-card-title">Data Member</h4>
							        </div><!--//col-->
							        
						        </div><!--//row-->
					        </div><!--//app-card-header-->
					        <div class="app-card-body p-3 p-lg-4">
								<form class="settings-form" id="MemberForm" name="MemberForm">
									<input type="hidden" name="id" class="form-control" id="setting-input-2" value="{{ $data->id }}" required="">
									<div class="mb-3">
									    <label for="setting-input-2" class="form-label">Nama Lengkap</label>
									    <input type="text" name="namalengkap" class="form-control" id="setting-input-2" value="{{ $data->namalengkap }}" required="">
									</div>
								    <div class="mb-3">
									    <label for="setting-input-3" class="form-label">Nomor Whatsapp</label>
									    <input type="text" name="nomorwa" class="form-control" id="setting-input-3" value="{{ $data->nomorwa }}" required>
									</div>
									<div class="mb-3">
									    <label for="setting-input-3" class="form-label">Kode Masuk</label>
									    <input type="text" name="otp" class="form-control" id="setting-input-3" value="{{ $data->otp }}" readonly>
									</div>
									<div class="mb-3">
									    <label for="setting-input-3" class="form-label">Alamat</label>
										<textarea rows="4" cols="50" name="alamat" class="form-control" required>{{ $data->alamat }}</textarea>
									    
									</div>
									<div class="mb-3">
									    <label for="setting-input-3" class="form-label">Desa/Kelurahan</label>
									    <input type="text" name="desa" class="form-control" id="setting-input-3" value="{{ $data->desa }}" required>
									</div>
									<div class="row g-4 mb-4">
										<div class="col-12 col-lg-4">
											<div class="mb-3">
												<label for="setting-input-3" class="form-label">Provinsi</label>
												<select class="form-select js-example-basic-single"  id="provinsi" name="prov">
													@foreach ($provinsi as $provinsi)
													<option value="{{ $provinsi->id_wilayah }}" {{ $data->prov==$provinsi->id_wilayah
														? 'selected' : '' }}>
														{{ $provinsi->nama_wilayah }}
													@endforeach
																			
											  </select>
											</div>
										</div>
										<div class="col-12 col-lg-4">
											<div class="mb-3">
												<label for="setting-input-3" class="form-label">Kabupaten</label>
												<select class="form-select js-example-basic-single" id="kabupaten" name="kab">
													@foreach ($kabupaten as $kota)
													<option value="{{ $kota->id_wilayah }}" {{ $data->kab==$kota->id_wilayah
														? 'selected' : '' }}>
														{{ $kota->nama_wilayah }}
													@endforeach
																			
											  </select>
											</div>
										</div>
										<div class="col-12 col-lg-4">
											<div class="mb-3">
												<label for="setting-input-3" class="form-label">Kecamatan</label>
												<select class="form-select js-example-basic-single" id="kecamatan" name="kec">
													@foreach ($kecamatan as $kecamatan)
													<option value="{{ $kecamatan->id_wilayah }}" {{ $data->kec==$kecamatan->id_wilayah
														? 'selected' : '' }}>
														{{ $kecamatan->nama_wilayah }}
													@endforeach
																			
											  </select>
											</div>
										</div>
									</div>

									
									<button type="submit" id="saveBtn" class="btn app-btn-primary">Simpan Perubahan</button>
							    </form>
					        </div><!--//app-card-body-->
				        </div><!--//app-card-->
			        </div><!--//col-->
			    
			        
			    </div><!--//row-->
			
			    
		    </div><!--//container-fluid-->
	    </div><!--//app-content-->
@endsection


@push('page-stylesheet')
<style>

.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #5d6778 !important;
    line-height: 28px !important;
    padding: 6px !important;
    font-size: 16px;
    margin-left: 3px !important;
}

.select2-container--default .select2-selection--single {
    background-color: var(--bs-body-bg) !important;
    border: 1px solid #e7e9ed !important;
    border-radius: 4px !important;
    height: 2.5rem !important;
    color: #5d6778;
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 26px !important;
    position: absolute !important;
    top: 5px !important;
    right: 1px !important;
    width: 20px !important;
}


</style>
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    $(document).ready(function() {


        $('#provinsi').on('change', function() {
            let data = {
                provinsi: $(this).val(),
            }

            $.ajax({
                type: 'get',
                url: 'ref-wilayah-kota',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#kabupaten').html('');
                    

                },
                success: function(response) {
                    $('#level3').show('');
                    $.each(response.data, function(i, val) {
                        $('#kabupaten').append(
                            `
                            <option value="${val.id_wilayah}">${val.nama_wilayah}</option>
                            `
                        );
                    });
                    $('#kabupaten').selectpicker('refresh');
                    $('#kabupaten').selectpicker('render');
                }
            });
        });

        $('#kabupaten').on('change', function() {
            let data = {
                kabupaten: $(this).val(),
            }

            $.ajax({
                type: 'get',
                url: 'ref-wilayah-kecamatan',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#kecamatan').html('');
                    
                },
                success: function(response) {
                    $('#level4').show('');
                    $.each(response.data, function(i, val) {
                        $('#kecamatan').append(
                            `
                            <option value="${val.id_wilayah}">${val.nama_wilayah}</option>
                            `
                        );
                    });
                    $('#kecamatan').selectpicker('refresh');
                    $('#kecamatan').selectpicker('render');
                }
            });
        });
/* 
        $('#kecamatan').on('change', function() {
            let data = {
                kecamatan: $(this).val(),
            }

            $('#wilayah').html(data.kecamatan);
            document.getElementById("wilayah").value=data.kecamatan;
        }); */
    });
</script>

</script>

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

      $('#saveBtn').click(function (e) {
          e.preventDefault();
          $('#saveBtn').html('Mengirim Data ..');
		  $('#saveBtn').attr('disabled','disabled');

          $.ajax({
            data: $('#MemberForm').serialize(),
            url: "update-member",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                $('#MemberForm').trigger("reset");
                if (data.status=='200')
                {
                    Swal.fire(
                    'Update Berhasil',
                    data.success,
                    'success'
                ).then(function (result) {
                if (result.value) {
                    window.location = "member";
                }
            })
                }else
                {
                    Swal.fire(
                    'Update Tidak Berhasil',
                    data.success,
                    'error'
                ).then(function (result) {
                if (result.value) {
                    
                }
            })
                }
               
                
                $('#saveBtn').html('Daftar');
				$('#saveBtn').removeAttr('disabled');
        
            },
            error: function (data) {
                //console.log('Error:', data);
                Swal.fire(
                    'Terdapat Kesalahan',
                    data.responseJSON.message,
                    'error'
                )
                $('#saveBtn').html('Daftar');
				$('#saveBtn').removeAttr('disabled');
            }
        });
      });


    });
  </script>

@endpush
