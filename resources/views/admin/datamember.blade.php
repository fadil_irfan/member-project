@extends('admin.layout')
@section('content')
<div class="app-content pt-3 p-md-3 p-lg-4">
		    <div class="container-xl">

			    <div class="row g-4 mb-4">

			        <div class="col-12 col-lg-12">
				        <div class="app-card app-card-stats-table h-100 shadow-sm">
					        <div class="app-card-header p-3">
						        <div class="row justify-content-between align-items-center">
							        <div class="col-auto">
						                <h4 class="app-card-title">Data Member</h4>
							        </div><!--//col-->

						        </div><!--//row-->
					        </div><!--//app-card-header-->
					        <div class="app-card-body p-3 p-lg-4">
						        <div class="table-responsive">
                                    <table id="refMember" class="table app-table-hover mb-0 text-left" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Lengkap</th>
                                            <th>Nomor Whatsapp</th>
                                            <th>Alamat</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                    </table>

                                </div><!--//table-responsive-->
					        </div><!--//app-card-body-->
				        </div><!--//app-card-->
			        </div><!--//col-->
			    </div><!--//row-->


		    </div><!--//container-fluid-->
	    </div><!--//app-content-->
@endsection
@push('page-script')
    <style type="text/css">
        .dataTables_filter input {
            width: 300px !important;
        }
        table.dataTable thead tr {
            background-color: #28a745 !important;
            color:white !important;
        }
    </style>

@endpush


@push('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('.file-upload-browse').on('click', function() {
                let file = $(this).parent().parent().parent().find('.file-upload-default');
                file.trigger('click');
            });
            $('.file-upload-default').on('change', function() {
                $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            });

            let table = $('#refMember').DataTable({
                ajax: {
                    url: 'getmember'
                },
                columns: [
                    { data: null, orderable: false, searchable: false },
                    { data: 'namalengkap' },
                    { data: 'nomorwa' },
                    { data: 'alamat' },
                    { data: 'id' },


                ],
                'columnDefs': [
                    {
                        "targets": 4,
                        "className": "text-center",
                        "render": function (data, type, row, meta) {
                            return "<a href='javascript:void(0)' class='btn app-btn-primary' >Edit</a> <a href='javascript:void(0)'  class='btn app-btn-primary' >Hapus</a>";
                        }
                    },
                ]

            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i+1;
                });
            });
            $('#refMember').on('click', '.deleteUser', function () {

                var Customer_id = $(this).data("id");
                Swal.fire({
                    icon: 'question',
                    title: 'Apakah anda yakin akan mengkonfirmasi pembayaran ?',
                    showCancelButton: true,
                    cancelButtonText:'Tidak',
                    confirmButtonText: 'Ya',
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "POST",
                            url: "verifikasi"+'/'+Customer_id,
                            success: function (data) {
                                table.ajax.url('konfirmasi-bayar').load();
                                Swal.fire(data.success, '', 'success')
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });

                    } else if (result.isDenied) {
                        Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
                    }
                })


            });


            $('#refMember').on('click', '.reminderUser', function () {

                var Customer_id = $(this).data("id");
                Swal.fire({
                    icon: 'question',
                    title: 'Ingatkan Calon Mahasiswa Baru tentang Pembayaran ?',
                    showCancelButton: true,
                    cancelButtonText:'Tidak',
                    confirmButtonText: 'Ya',
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        $.ajax({
                            type: "POST",
                            url: "reminder"+'/'+Customer_id,
                            success: function (data) {
                                table.ajax.url('konfirmasi-bayar').load();
                                Swal.fire(data.success, '', 'success')
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            }
                        });

                    } else if (result.isDenied) {
                        Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
                    }
                })


            });
        });



    </script>
@endpush

