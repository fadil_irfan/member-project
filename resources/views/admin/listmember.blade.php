@extends('admin.layout')
@section('content')
<div class="app-content pt-3 p-md-3 p-lg-4">
		    <div class="container-xl">

			    <div class="row g-4 mb-4">

			        <div class="col-12 col-lg-12">
				        <div class="app-card app-card-stats-table h-100 shadow-sm">
					        <div class="app-card-header p-3">
						        <div class="row justify-content-between align-items-center">
							        <div class="col-auto">
						                <h4 class="app-card-title">List Member</h4>
							        </div><!--//col-->

						        </div><!--//row-->
					        </div><!--//app-card-header-->
					        <div class="app-card-body p-3 p-lg-4">
						        <div class="table-responsive">
                                    <table id="refMember" class="table app-table-hover mb-0 text-left" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Cek</th>
                                            <th>Nama Lengkap</th>
                                            <th>Nomor Whatsapp</th>
                                            <th>Alamat</th>

                                        </tr>
                                        </thead>
                                    </table>
                                    <p class="form-group">
                                        <button type="submit" class="btn app-btn-primary" id="kirim">Simpan Nomor Terpilih</button>
                                    </p>


                                </div><!--//table-responsive-->
					        </div><!--//app-card-body-->
				        </div><!--//app-card-->
			        </div><!--//col-->
			    </div><!--//row-->


		    </div><!--//container-fluid-->
	    </div><!--//app-content-->
@endsection
@push('page-stylesheet')


@endpush


@push('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.12/js/dataTables.checkboxes.min.js"></script>


    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('.file-upload-browse').on('click', function() {
                let file = $(this).parent().parent().parent().find('.file-upload-default');
                file.trigger('click');
            });
            $('.file-upload-default').on('change', function() {
                $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
            });

            let table = $('#refMember').DataTable({
                ajax: {
                    url: '/getmember'
                },
                columns: [
                    { data: null, orderable: false, searchable: false },
                    { data: 'id' },
                    { data: 'namalengkap' },
                    { data: 'nomorwa' },
                    { data: 'alamat' },
                ],
                'columnDefs': [
                    {
                        'targets': 1,
                        'checkboxes': {
                            'selectRow': true
                        }
                    },


                ],
                'select': {
                    'style': 'multi'
                },


            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = i+1;
                });
            });


            $("#kirim").click(function (e) {
                e.preventDefault();
                var rows_selected = table.column(1).checkboxes.selected();
                var id = "{{ Request::segment(2) }}";

                if (rows_selected !== null)
                {
                    $.ajax({
                        type: 'POST',
                        url: "{{ url('selectmember') }}",
                        data: {
                            id_member: rows_selected.join(","),
                            id_broadcast:id
                            //id_broadcast: e.options[e.selectedIndex].value
                        },
                        success: function (data) {
                            if (data.status=='200')
                            {
                                Swal.fire(
                                    'Tambah Data Berhasil',
                                    data.success,
                                    'success'
                                ).then(function (result) {
                                    if (result.value) {
                                        window.location = "/broadcast";
                                    }
                                })
                            }else
                            {
                                Swal.fire(
                                    'Tambah Data Tidak Berhasil',
                                    data.success,
                                    'error'
                                ).then(function (result) {
                                    if (result.value) {

                                    }
                                })
                            }


                        },
                        error: function (response) {
                            Swal.fire(
                                'Terdapat Kesalahan',
                                data.responseJSON.message,
                                'error'
                            )
                        }
                    });

                }else {
                    Swal.fire(
                        'Terdapat Kesalahan',
                        'Member Harus Dipilih Minimal Satu',
                        'error'
                    )
                }

                /* // Iterate over all selected checkboxes
                $.each(rows_selected, function (index, rowId) {
                    // Create a hidden element
                    $(form).append(
                        $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'id[]')
                        .val(rowId)
                    );
                }); */

            });

            });



    </script>
@endpush

