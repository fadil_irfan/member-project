@extends('admin.layout')
@section('content')
<div class="app-content pt-3 p-md-3 p-lg-4">
		    <div class="container-xl">

			    <div class="row g-4 mb-4">

			        <div class="col-12 col-lg-12">
				        <div class="app-card app-card-stats-table h-100 shadow-sm">
					        <div class="app-card-header p-3">
						        <div class="row justify-content-between align-items-center">
							        <div class="col-auto">
						                <h4 class="app-card-title">Form Broadcast Pesan</h4>


							        </div><!--//col-->

                                    <br/>
                                    <br/>

                                </div><!--//row-->
					        </div><!--//app-card-header-->
					        <div class="app-card-body p-3 p-lg-4">
                                <div class="col-12 col-md-12">
                                    <div class="app-card app-card-settings shadow-sm p-4">

                                        <div class="app-card-body">
                                            <form class="settings-form" id="MemberForm">
                                                <div class="mb-3">
                                                    <label for="setting-input-1" class="form-label">Nama Broadcast<span class="ms-2" data-bs-container="body" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-placement="top" data-bs-content="This is a Bootstrap popover example. You can use popover to provide extra info."><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-info-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
  <path d="M8.93 6.588l-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588z"></path>
  <circle cx="8" cy="4.5" r="1"></circle>
</svg></span></label>
                                                    <input type="text" class="form-control" name="namabroadcast" id="setting-input-1" required="">
                                                </div>
                                                <div class="mb-3">
                                                    <label for="setting-input-2" class="form-label">Pesan</label>
                                                    <textarea class="form-control" rows="10" name="pesanbroadcast" style="height:100%;"></textarea>
                                                </div>

                                                <button type="submit" id="saveBtn" class="btn app-btn-primary">Simpan</button>
                                            </form>
                                        </div><!--//app-card-body-->

                                    </div><!--//app-card-->
                                </div>
					        </div><!--//app-card-body-->
				        </div><!--//app-card-->
			        </div><!--//col-->
			    </div><!--//row-->


		    </div><!--//container-fluid-->
	    </div><!--//app-content-->
@endsection



@push('page-script')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                $('#saveBtn').html('Mengirim Data ..');
                $('#saveBtn').attr('disabled','disabled');

                $.ajax({
                    data: $('#MemberForm').serialize(),
                    url: "simpan-broadcast",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#MemberForm').trigger("reset");
                        if (data.status=='200')
                        {
                            Swal.fire(
                                'Tambah Data Berhasil',
                                data.success,
                                'success'
                            ).then(function (result) {
                                if (result.value) {
                                    window.location = "broadcast";
                                }
                            })
                        }else
                        {
                            Swal.fire(
                                'Tambah Data Tidak Berhasil',
                                data.success,
                                'error'
                            ).then(function (result) {
                                if (result.value) {

                                }
                            })
                        }


                        $('#saveBtn').html('Simpan');
                        $('#saveBtn').removeAttr('disabled');

                    },
                    error: function (data) {
                        //console.log('Error:', data);
                        Swal.fire(
                            'Terdapat Kesalahan',
                            data.responseJSON.message,
                            'error'
                        )
                        $('#saveBtn').html('Simpan');
                        $('#saveBtn').removeAttr('disabled');
                    }
                });
            });
        });



    </script>
@endpush

