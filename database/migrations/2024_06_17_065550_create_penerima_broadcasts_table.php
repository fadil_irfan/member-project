<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penerima_broadcasts', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('idmember');
            $table->uuid('idbroadcast');
            $table->string('status')->nullable();
            $table->tinyText('responpengiriman')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerima_broadcasts');
    }
};
