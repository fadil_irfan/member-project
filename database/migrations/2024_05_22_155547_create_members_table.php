<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->uuid('id')->primary(); 
            $table->string('namalengkap');
            $table->string('nomorwa');
            $table->tinyText('alamat')->nullable();
            $table->string('desa')->nullable();
            $table->string('prov');
            $table->string('kab');
            $table->string('kec');
            $table->string('otp');
            $table->string('is_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
};
